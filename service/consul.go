package service

import (
	"fmt"
	"log"

	"git.acwing.com/1318686278/easygo/consul"
	"git.acwing.com/1318686278/easygo/model"
)

const (
	serviceName = "tyl.blog.search"
	localPort   = 7004
)

func InitConsul() {
	checkUrl := fmt.Sprintf("http://%s:%d/search/ping", model.AliyunLocalIp, localPort)
	err := consul.RegisterConsul(serviceName, localPort, checkUrl)
	if err != nil {
		log.Fatal(err.Error())
	}
}
