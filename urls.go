package main

import (
	"fmt"
	"net/http"
	"search/controller"
	"search/dao"
	"search/server"
	"search/tyl/blog/search"
	"strings"
	"time"

	"git.acwing.com/1318686278/easygo/metric"
	"git.acwing.com/1318686278/easygo/middleware"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"google.golang.org/grpc"
)

var openApi = map[string]http.HandlerFunc{
	"/ping": controller.Ping,
}

func InitServer() *http.Server {
	preFn := func(path string, handler func(http.ResponseWriter, *http.Request)) func(w http.ResponseWriter, r *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			logid := r.Header.Get("X-Log-Id")
			if logid == "" {
				logid = r.URL.Query().Get("X-Log-Id")
			}
			r.Header.Set("X-Log-Id", logid)
			handler(w, r)
		}
	}

	//http
	httpmux := http.NewServeMux()
	prefix := "/search"
	for path, handler := range openApi {
		fn := handler
		if path != "/ping" {
			fn = preFn(path, handler)
		}
		fn = middleware.DefaultRateLimit(50*time.Millisecond, 20, fn)
		fn = middleware.DefaultSetHeader(fn)
		httpmux.HandleFunc(prefix+path, fn)
	}

	// grpc
	grpcServer := grpc.NewServer()
	search.RegisterSearchServiceServer(grpcServer, new(server.SearchServiceServerImpl))

	//http2
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		logid := r.Header.Get("X-Log-Id")
		if logid == "" {
			logid = r.FormValue("X-Log-Id")
		}

		if r.ProtoMajor != 2 {
			httpmux.ServeHTTP(w, r)
			metric.DefaultMetric(logid, "search", r.RequestURI, r)
			return
		}
		if strings.Contains(r.Header.Get("Content-Type"), "application/grpc") {
			grpcServer.ServeHTTP(w, r) // gRPC Server
			return
		}
		httpmux.ServeHTTP(w, r)
	})

	h2Handler := h2c.NewHandler(mux, &http2.Server{})

	port := fmt.Sprintf(":%d", dao.Conf.Port)
	server := &http.Server{Addr: port, Handler: h2Handler}
	return server
}
