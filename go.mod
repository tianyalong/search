module search

go 1.16

require (
	git.acwing.com/1318686278/easygo v1.0.45
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/olivere/elastic/v7 v7.0.31
	golang.org/x/net v0.0.0-20211209124913-491a49abca63
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.27.1
	gopkg.in/ini.v1 v1.66.3
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
