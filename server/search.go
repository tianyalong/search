package server

import (
	"context"
	"encoding/json"
	"search/elasticsearch"
	"search/tyl/blog/search"

	"git.acwing.com/1318686278/easygo/metric"
	"google.golang.org/grpc/metadata"
)

type SearchServiceServerImpl struct{}

func (p *SearchServiceServerImpl) SeniorSearch(c context.Context, r *search.SearchRequest) (*search.SearchResponse, error) {
	//metric
	md, ok := metadata.FromIncomingContext(c)
	if ok {
		metric.RpcMetric(md.Get("X-Log-Id")[0], "text="+r.GetText())
	}

	//work
	text := r.GetText()
	nodes := elasticsearch.GetBlogsWithHighLight(text)
	nodesByte, err := json.Marshal(&nodes)
	if err != nil {
		metric.ErrorMetric(md.Get("X-Log-Id")[0], err.Error())
		return &search.SearchResponse{ArrayBytes: []byte{}}, err
	}
	return &search.SearchResponse{ArrayBytes: nodesByte}, nil
}
