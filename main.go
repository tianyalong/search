package main

import (
	"log"
	"search/dao"
	"search/elasticsearch"
	"search/service"

	"git.acwing.com/1318686278/easygo/metric"
	"git.acwing.com/1318686278/easygo/middleware"
)

func main() {
	metric.Init("search")
	middleware.InitLog()
	dao.InitDBGorm()
	elasticsearch.InitES()
	elasticsearch.InitSync()
	service.InitConsul()
	if err := InitServer().ListenAndServe(); err != nil {
		log.Fatal(err.Error())
	}
}
