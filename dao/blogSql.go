package dao

import (
	"log"
	"search/model"

	emodel "git.acwing.com/1318686278/easygo/model"
)

func GetBlogById(id uint64) (model.Blog, error) {
	blog := model.Blog{}
	res := GormDB.Model(&model.Blog{}).Where("id = ?", id).Find(&blog)
	if res.Error != nil {
		log.Println(emodel.Error + "GetBlogById err:" + res.Error.Error())
		return model.Blog{}, res.Error
	}
	return blog, nil
}

func InsertBlog(blog *model.Blog) error {
	res := GormDB.Create(blog)
	if res.Error != nil {
		log.Println(emodel.Error + "InsertBlog err:" + res.Error.Error())
		return res.Error
	}
	return nil
}

func UpdateBlog(blog *model.Blog) error {
	res := GormDB.Model(&model.Blog{}).Where("id = ?", blog.ID).Updates(&model.Blog{
		Title:        blog.Title,
		MarkDownText: blog.MarkDownText,
		HtmlText:     blog.HtmlText,
		Extra:        blog.Extra,
	})
	if res.Error != nil {
		log.Println(emodel.Error + "UpdateBlog err:" + res.Error.Error())
		return res.Error
	}
	return nil
}

// GetAllBlogV1Sql 获取所有文章，不分类型
func GetAllBlogV1Sql() (blogs []model.Blog, sum int64, err error) {
	//没命中缓存，访问db
	res := GormDB.Model(&model.Blog{}).Count(&sum).Find(&blogs)
	if res.Error != nil {
		log.Println(emodel.Error + "GetAllBlogV1Sql err:" + res.Error.Error())
		return blogs, -1, res.Error
	}
	return blogs, sum, nil
}

// 获取所有文章的简介
func GetAllBlogsES() (blogs []model.Blog, err error) {
	res := GormDB.Select("id", "title", "extra", "created_at", "mark_down_text").Find(&blogs)
	if res.Error != nil {
		log.Println(emodel.Error + res.Error.Error())
		return blogs, res.Error
	}
	return blogs, nil
}
