package dao

import (
	"fmt"
	"log"

	emodel "git.acwing.com/1318686278/easygo/model"
	"gopkg.in/ini.v1"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type AppConfig struct {
	Release      bool `ini:"release"`
	Port         int  `ini:"port"`
	*MySqlConfig `ini:"mysql"`
}

type MySqlConfig struct {
	User     string `ini:"user"`
	Password string `ini:"password"`
	DB       string `ini:"db"`
	Host     string `ini:"host"`
	Port     int    `ini:"port"`
}

var Conf AppConfig

var GormDB *gorm.DB

func InitDBGorm() {
	file := "./conf/config.ini"
	err := ini.MapTo(&Conf, file)
	if err != nil {
		log.Println(emodel.Critical + "read config err:" + err.Error())
		return
	}

	//dsn := "root:z2000w06x02@(127.0.0.1:3306)/tyl?charset=utf8mb4&parseTime=True&loc=Local"
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", Conf.User, Conf.Password, Conf.Host, Conf.MySqlConfig.Port, Conf.DB)
	GormDB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalln(emodel.Critical + "gorm连接数据库失败")
	}
}
