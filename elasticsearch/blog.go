package elasticsearch

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"search/dao"
	"search/model"
	"strconv"
	"strings"
	"time"

	emodel "git.acwing.com/1318686278/easygo/model"
	"git.acwing.com/1318686278/easygo/mycall"
)

const (
	indexName = "blog"
)

func CreateIndex() {
	esclient.CreateIndex(indexName).Do(context.Background())
}

func InitSync() {
	SyncBlogs()
	go func() {
		ticker := time.NewTicker(time.Minute)
		for {
			select {
			case <-ticker.C:
				SyncBlogs()
			default:
				time.Sleep(100 * time.Millisecond)
			}
		}
	}()
}

func SyncBlogs() {
	blogs, err := dao.GetAllBlogsES()
	if err != nil {
		log.Println(emodel.Error + err.Error())
	}

	for _, blog := range blogs {
		extra, err := model.MarshalBlogExtra(blog.Extra)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		data := model.BlogEs{
			ID:        blog.ID,
			Title:     blog.Title,
			Type:      extra.Type,
			CreatedAt: blog.CreatedAt.String(),
			MkText:    blog.MarkDownText,
		}
		_, err = esclient.Index().Index(indexName).Type("_doc").Id(strconv.Itoa(int(data.ID))).BodyJson(data).Do(context.Background())
		if err != nil {
			log.Println(emodel.Error + err.Error())
		}
	}
}

func GetBlogsWithHighLight(text string) (nodes []model.RpcNode) {
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"mk_text": text,
			},
		},
		"highlight": map[string]interface{}{
			"fields": map[string]interface{}{
				"mk_text": map[string]interface{}{},
			},
		},
	}
	queryByte, _ := json.Marshal(query)
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	res, err := mycall.SendHttpReq("GET", "http://8.142.97.150:9200/blog/_search", headers, bytes.NewReader(queryByte))
	if err != nil {
		log.Println(err.Error())
	}
	response := model.EsResponse{}
	json.Unmarshal(res, &response)

	for _, article := range response.Hits.Hits {
		highlight := ""
		for i := 0; i < len(article.Highlight.MkText); i++ {
			if i == 3 {
				break
			}
			sentence := article.Highlight.MkText[i]
			sentence = strings.ReplaceAll(sentence, "<code>", "")
			sentence = strings.ReplaceAll(sentence, "</code>", "")
			sentence = sentence + "<br>"
			highlight += sentence
		}
		node := model.RpcNode{
			ID:        uint(article.Source.ID),
			Title:     article.Source.Title,
			CreateAt:  article.Source.CreateAt,
			HighLight: highlight,
		}
		nodes = append(nodes, node)
	}
	return
}
