package elasticsearch

import (
	"context"
	"log"

	"github.com/olivere/elastic/v7"
)

var esclient *elastic.Client

var host = "http://8.142.97.150:9200/"

func InitES() {
	if esclient != nil {
		return
	}

	var err error
	esclient, err = elastic.NewClient(elastic.SetSniff(false), elastic.SetURL(host))
	if err != nil {
		log.Fatal(err.Error())
	}

	_, code, err := esclient.Ping(host).Do(context.Background())
	if err != nil || code != 200 {
		log.Fatal(err.Error())
	}
}
