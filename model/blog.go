package model

import (
	"encoding/json"
	"log"

	emodel "git.acwing.com/1318686278/easygo/model"
	"gorm.io/gorm"
)

const (
	Type_blog      = 0 //博客
	Type_note      = 1 //笔记
	Type_collect   = 2 //收藏
	Type_interview = 3 //面试
)

type Blog struct {
	gorm.Model
	Title        string `gorm:"type:varchar(120)"`
	MarkDownText string `gorm:"type:longtext"`
	HtmlText     string `gorm:"type:longtext"`
	Extra        string `gorm:"type:longtext"`
}

type BlogEs struct {
	ID        uint   `json:"id"`
	Title     string `json:"title"`
	Type      int    `json:"type"`
	CreatedAt string `json:"create_at"`
	MkText    string `json:"mk_text"`
}

type BlogExtra struct {
	Type        int  `json:"type"`
	AccessCnt   uint `json:"access_cnt"`
	FabulousCnt uint `json:"fabulous_cnt"`
}

func MarshalBlogExtra(s string) (extra BlogExtra, err error) {
	err = json.Unmarshal([]byte(s), &extra)
	if err != nil {
		log.Println(emodel.Error + err.Error())
		return BlogExtra{}, err
	}
	return extra, nil
}

type RpcNode struct {
	ID        uint   `json:"id"`
	Title     string `json:"title"`
	CreateAt  string `json:"create_at"`
	HighLight string `json:"high_light"`
}

type EsResponse struct {
	Took     int  `json:"took"`
	TimedOut bool `json:"timed_out"`
	Shards   struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Skipped    int `json:"skipped"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
	Hits struct {
		Total struct {
			Value    int    `json:"value"`
			Relation string `json:"relation"`
		} `json:"total"`
		MaxScore float64 `json:"max_score"`
		Hits     []struct {
			Index   string   `json:"_index"`
			Type    string   `json:"_type"`
			ID      string   `json:"_id"`
			Score   float64  `json:"_score"`
			Ignored []string `json:"_ignored,omitempty"`
			Source  struct {
				ID       int    `json:"id"`
				Title    string `json:"title"`
				Type     int    `json:"type"`
				CreateAt string `json:"create_at"`
				MkText   string `json:"mk_text"`
			} `json:"_source"`
			Highlight struct {
				MkText []string `json:"mk_text"`
			} `json:"highlight"`
		} `json:"hits"`
	} `json:"hits"`
}
